import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:h2bet/app/pages/widgets/h2bet_appbar.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale.dart';
import 'package:h2bet/app/theme/h2bet_colors.dart';
import 'package:h2bet/app/theme/h2bet_style.dart';

@RoutePage()
class ExitH2BetPage extends StatelessWidget {
  const ExitH2BetPage({super.key});

  @override
  Widget build(BuildContext context) {
    return PageH2BetWrapper(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          H2BetAppBar(
            onTapBackButton: () => context.router.pop(),
          ),
          const SizedBox(
            height: 60,
          ),
          Center(
            child: Text(
              AppLocalizations.of(context)!.h2betExit,
              style: H2BETStyle.h2betLoadingStyle,
            ),
          ),
          const SizedBox(
            height: 120,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _ExitH2BetButton(
                buttonName: AppLocalizations.of(context)!.h2betYes,
                onTap: () => SystemNavigator.pop(),
              ),
              const SizedBox(
                width: 30,
              ),
              _ExitH2BetButton(
                buttonName: AppLocalizations.of(context)!.h2betNo,
                onTap: () => context.router.pop(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _ExitH2BetButton extends StatelessWidget {
  final String buttonName;
  final VoidCallback onTap;

  const _ExitH2BetButton({
    required this.buttonName,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final buttonBorderRadius = BorderRadius.circular(30);
    return ClipRRect(
      borderRadius: buttonBorderRadius,
      child: InkWell(
        borderRadius: buttonBorderRadius,
        radius: 30,
        splashColor: Colors.transparent,
        onTap: onTap,
        child: Container(
          color: H2BETColors.defaultH2BetColor,
          width: 120,
          height: 65,
          child: Center(
            child: Text(
              buttonName,
              style: H2BETStyle.h2betMenuButtonStyle,
            ),
          ),
        ),
      ),
    );
  }
}
