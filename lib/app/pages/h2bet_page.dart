import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:h2bet/app/theme/h2bet_colors.dart';
import 'package:h2bet/core/cubit/h2bet_cubit/h2bet_cubit.dart';
import 'package:h2bet/core/cubit/h2bet_cubit/h2bet_state.dart';
import 'package:h2bet/core/router/h2bet_router.dart';
import 'package:image_picker/image_picker.dart';
import 'package:webview_flutter/webview_flutter.dart';
// ignore: depend_on_referenced_packages
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
// ignore: depend_on_referenced_packages
import 'package:webview_flutter_android/webview_flutter_android.dart';

@RoutePage()
class H2BetPage extends StatefulWidget {
  final String h2betLink;

  const H2BetPage({
    super.key,
    required this.h2betLink,
  });

  @override
  State<H2BetPage> createState() => _H2BetPageState();
}

class _H2BetPageState extends State<H2BetPage> {
  final _cookieManager = WebViewCookieManager();
  late WebViewController _h2betViewController;
  late H2BetCubit _h2betCubit;

  @override
  void initState() {
    _h2betCubit = H2BetCubit();
    _h2betCubit.updateH2BetLink(widget.h2betLink);
    _h2betViewController = _getH2BetWebViewController();
    _onSetCookie();
    _h2betPhotoSelection();
    super.initState();
  }

  Future<void> _onSetCookie() async {
    final uriH2Bet = Uri.parse(widget.h2betLink);
    final String domainH2Bet = uriH2Bet.host;
    await _cookieManager.setCookie(
      WebViewCookie(
        name: 'foo',
        value: 'bar',
        domain: domainH2Bet,
        path: '/',
      ),
    );
    await _h2betViewController.loadRequest(uriH2Bet);
  }

  WebViewController _getH2BetWebViewController() {
    late final PlatformWebViewControllerCreationParams params;
    params = WebViewPlatform.instance is WebKitWebViewPlatform
        ? WebKitWebViewControllerCreationParams(
            allowsInlineMediaPlayback: true,
            mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{})
        : const PlatformWebViewControllerCreationParams();

    final h2BetWebViewContorller = WebViewController.fromPlatformCreationParams(
      params,
      onPermissionRequest: (request) async {
        request.grant();
      },
    );
    h2BetWebViewContorller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..enableZoom(true)
      ..setNavigationDelegate(
        NavigationDelegate(
          onPageFinished: (url) async {
            final cookies =
                await _h2betViewController.runJavaScriptReturningResult(
              'document.cookie',
            );
            _h2betCubit.saveH2BetCookie(cookies.toString());
            return _h2betCubit.finishLoad();
          },
        ),
      );

    return h2BetWebViewContorller;
  }

  void _h2betPhotoSelection() async {
    if (Platform.isAndroid) {
      final h2betController =
          _h2betViewController.platform as AndroidWebViewController;
      await h2betController.setOnShowFileSelector(_h2betPhotoPicker);
    }
  }

  Future<List<String>> _h2betPhotoPicker(FileSelectorParams params) async {
    try {
      if (params.acceptTypes.any((type) => type == 'image/*')) {
        final pickerH2Bet = ImagePicker();
        final photoH2Bet =
            await pickerH2Bet.pickImage(source: ImageSource.gallery);

        if (photoH2Bet == null) {
          return [];
        }
        return [Uri.file(photoH2Bet.path).toString()];
      }
      if (params.mode == FileSelectorMode.openMultiple) {
        final attachmentH2Bet =
            await FilePicker.platform.pickFiles(allowMultiple: true);
        if (attachmentH2Bet == null) return [];

        return attachmentH2Bet.files
            .where((element) => element.path != null)
            .map((e) => File(e.path!).uri.toString())
            .toList();
      } else {
        final attachmentH2Bet = await FilePicker.platform.pickFiles();
        if (attachmentH2Bet == null) return [];
        File file = File(attachmentH2Bet.files.single.path!);
        return [file.uri.toString()];
      }
    } catch (e) {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: PageH2BetWrapper(
        child: BlocConsumer<H2BetCubit, H2BetState>(
          bloc: _h2betCubit,
          listener: (context, state) {
            if (!state.isHavaInternet) {
              context.router.replace(const MenuH2BETRoute());
            }
          },
          builder: (context, state) {
            return state.isLoading
                ? Center(
                    child: CircularProgressIndicator(
                      color: H2BETColors.matchButtonColor,
                    ),
                  )
                : WebViewWidget(
                    controller: _h2betViewController,
                  );
          },
        ),
      ),
    );
  }
}
