import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:h2bet/app/pages/widgets/h2bet_appbar.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:h2bet/app/theme/h2bet_colors.dart';
import 'package:h2bet/app/theme/h2bet_style.dart';
import 'package:h2bet/core/cubit/history_football_teams_cubit/history_teams_cubit.dart';
import 'package:h2bet/core/cubit/history_football_teams_cubit/history_teams_state.dart';
import 'package:h2bet/core/model/football_team_h2bet.dart';
import 'package:h2bet/core/router/h2bet_router.dart';

@RoutePage()
class HistoryMenuH2BetPage extends StatefulWidget {
  const HistoryMenuH2BetPage({super.key});

  @override
  State<HistoryMenuH2BetPage> createState() => _HistoryMenuH2BetPageState();
}

class _HistoryMenuH2BetPageState extends State<HistoryMenuH2BetPage> {
  late HistoryTeamsCubit _historyTeamsCubit;

  @override
  void initState() {
    _historyTeamsCubit = HistoryTeamsCubit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PageH2BetWrapper(
      child: BlocConsumer<HistoryTeamsCubit, HistoryTeamsState>(
        bloc: _historyTeamsCubit,
        listener: (context, state) {},
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H2BetAppBar(
                onTapBackButton: () => context.router.pop(),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: state.footballTeamsH2Bet.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 30,
                      ),
                      child: _HistoryMenuButton(
                        onTap: () => context.router.navigate(
                          HistoryTeamH2BetRoute(
                            footballTeamH2Bet: state.footballTeamsH2Bet[index],
                          ),
                        ),
                        footballTeamH2Bet: state.footballTeamsH2Bet[index],
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class _HistoryMenuButton extends StatelessWidget {
  final FootballTeamH2Bet footballTeamH2Bet;
  final VoidCallback onTap;

  const _HistoryMenuButton({
    required this.onTap,
    required this.footballTeamH2Bet,
  });

  @override
  Widget build(BuildContext context) {
    final buttonBorderRadius = BorderRadius.circular(30);
    return ClipRRect(
      borderRadius: buttonBorderRadius,
      child: InkWell(
        borderRadius: buttonBorderRadius,
        radius: 30,
        splashColor: Colors.transparent,
        onTap: onTap,
        child: Container(
          color: H2BETColors.defaultH2BetColor,
          width: MediaQuery.of(context).size.width * 0.7,
          height: 70,
          child: Row(children: [
            const SizedBox(
              width: 20,
            ),
            Expanded(
              child: Text(
                footballTeamH2Bet.teamName,
                style: H2BETStyle.h2betMenuButtonStyle,
              ),
            ),
            Image.asset(
              footballTeamH2Bet.teamLogo,
              height: 50,
            ),
            const SizedBox(
              width: 20,
            ),
          ]),
        ),
      ),
    );
  }
}
