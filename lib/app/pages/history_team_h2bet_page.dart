import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:h2bet/app/pages/widgets/h2bet_appbar.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:h2bet/app/theme/h2bet_style.dart';
import 'package:h2bet/core/model/football_team_h2bet.dart';

@RoutePage()
class HistoryTeamH2BetPage extends StatefulWidget {
  final FootballTeamH2Bet footballTeamH2Bet;

  const HistoryTeamH2BetPage({
    super.key,
    required this.footballTeamH2Bet,
  });

  @override
  State<HistoryTeamH2BetPage> createState() => _HistoryTeamH2BetPageState();
}

class _HistoryTeamH2BetPageState extends State<HistoryTeamH2BetPage> {
  @override
  Widget build(BuildContext context) {
    return PageH2BetWrapper(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            H2BetAppBar(
              onTapBackButton: () => context.router.pop(),
            ),
            Center(
              child: Image.asset(
                widget.footballTeamH2Bet.teamLogo,
                height: 120,
              ),
            ),
            const SizedBox(
              height: 60,
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Text(
                  widget.footballTeamH2Bet.teamHistory,
                  style: H2BETStyle.h2betHistoryTeamStyle,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
