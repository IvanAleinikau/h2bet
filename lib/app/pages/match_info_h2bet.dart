import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:h2bet/app/pages/widgets/h2bet_appbar.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:h2bet/app/theme/h2bet_colors.dart';
import 'package:h2bet/app/theme/h2bet_style.dart';
import 'package:h2bet/core/model/match_h2bet.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale.dart';

@RoutePage()
class MatchInfoH2BetPage extends StatelessWidget {
  final MatchH2Bet matchH2Bet;
  final int matchIndex;

  const MatchInfoH2BetPage({
    super.key,
    required this.matchH2Bet,
    required this.matchIndex,
  });

  @override
  Widget build(BuildContext context) {
    return PageH2BetWrapper(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            H2BetAppBar(
              onTapBackButton: () => context.router.pop(),
            ),
            const SizedBox(
              height: 50,
            ),
            _InfoWrapper(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset(
                              matchH2Bet.firstFootballTeam.teamLogo,
                              height: 50,
                              width: 50,
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            DateFormat('d EEEE')
                                .format(matchH2Bet.matchDateTime)
                                .toLowerCase(),
                            style: H2BETStyle.h2betDateMatchStyle,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            '${12 + matchIndex}:00',
                            style: H2BETStyle.h2betTimeInfoStyle,
                          ),
                        ],
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Image.asset(
                              matchH2Bet.secondFootballTeam.teamLogo,
                              height: 50,
                              width: 50,
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        matchH2Bet.firstFootballTeam.teamName,
                        style: H2BETStyle.h2betMatchInfoStyle,
                      ),
                      Text(
                        matchH2Bet.secondFootballTeam.teamName,
                        style: H2BETStyle.h2betMatchInfoStyle,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: List.generate(
                          matchH2Bet.goalInfoFirstTeam.length,
                          (index) => Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child: _MatchGoal(
                              goal: matchH2Bet.goalInfoFirstTeam[index],
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: List.generate(
                          matchH2Bet.goalInfoSecondTeam.length,
                          (index) => Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: _MatchGoal(
                              goal: matchH2Bet.goalInfoSecondTeam[index],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            _InfoWrapper(
              child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!.whoWithH2Bet,
                    style: H2BETStyle.h2betMatchInfoSubtitleStyle,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: H2BETColors.h2betWhiteColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Image.asset(
                                matchH2Bet.firstFootballTeam.teamLogo,
                                height: 30,
                              ),
                              Text(
                                matchH2Bet.firstTeamCoefficient.toString(),
                                style: H2BETStyle.h2betCoeffStyle,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 60,
                      ),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: H2BETColors.h2betWhiteColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                matchH2Bet.secondTeamCoefficient.toString(),
                                style: H2BETStyle.h2betCoeffStyle,
                              ),
                              Image.asset(
                                matchH2Bet.secondFootballTeam.teamLogo,
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            _InfoWrapper(
              child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!.matchPreview,
                    style: H2BETStyle.h2betMatchInfoSubtitleStyle,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Text(
                    matchH2Bet.matchPreViewInformation,
                    style: H2BETStyle.h2betMatchInfoGoalStyle,
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }
}

class _InfoWrapper extends StatelessWidget {
  final Widget child;

  const _InfoWrapper({
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(
          color: H2BETColors.h2betWhiteColor,
        ),
        color: H2BETColors.infoWrapperH2BetColor,
      ),
      child: child,
    );
  }
}

class _MatchGoal extends StatelessWidget {
  final int goal;

  const _MatchGoal({
    required this.goal,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 22,
      width: 22,
      decoration: BoxDecoration(
        color: _getColor(),
        borderRadius: BorderRadius.circular(50),
        border: goal == 0
            ? Border.all(
                color: H2BETColors.borderGoalMark,
              )
            : null,
      ),
    );
  }

  Color _getColor() {
    switch (goal) {
      case 1:
        return H2BETColors.h2betGreenColor;
      case -1:
        return H2BETColors.h2betRedColor;
      case 2:
        return H2BETColors.h2betYellowColor;
      default:
        return H2BETColors.h2betTransparentColor;
    }
  }
}
