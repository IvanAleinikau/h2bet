import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:h2bet/app/pages/widgets/h2bet_appbar.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:h2bet/app/theme/h2bet_colors.dart';
import 'package:h2bet/app/theme/h2bet_style.dart';
import 'package:h2bet/core/cubit/matches_h2bet_cubit.dart/matches_h2bet_cubit.dart';
import 'package:h2bet/core/cubit/matches_h2bet_cubit.dart/matches_h2bet_state.dart';
import 'package:h2bet/core/model/match_h2bet.dart';
import 'package:h2bet/core/router/h2bet_router.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale.dart';

@RoutePage()
class MatchesH2BetPage extends StatefulWidget {
  const MatchesH2BetPage({super.key});

  @override
  State<MatchesH2BetPage> createState() => _MatchesH2BetPageState();
}

class _MatchesH2BetPageState extends State<MatchesH2BetPage> {
  late MatchesH2BetCubit _matchesH2BetCubit;

  @override
  void initState() {
    _matchesH2BetCubit = MatchesH2BetCubit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MatchesH2BetCubit, MatchesH2BetState>(
      bloc: _matchesH2BetCubit,
      listener: (context, state) {},
      builder: (context, state) {
        return PageH2BetWrapper(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H2BetAppBar(
                onTapBackButton: () => context.router.pop(),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: state.matchesH2Bet.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30,
                        vertical: 15,
                      ),
                      child:
                          _MatchH2BetWidget(state.matchesH2Bet[index], index),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class _MatchH2BetWidget extends StatelessWidget {
  final MatchH2Bet matchH2Bet;
  final int matchIndex;

  const _MatchH2BetWidget(this.matchH2Bet, this.matchIndex);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 30,
            ),
            height: 152,
            color: H2BETColors.matchesH2BetColor,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            matchH2Bet.firstFootballTeam.teamLogo,
                            height: 50,
                            width: 50,
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Text(
                          DateFormat('d EEEE')
                              .format(matchH2Bet.matchDateTime)
                              .toLowerCase(),
                          style: H2BETStyle.h2betDateMatchStyle,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          '${12 + matchIndex}:00',
                          style: H2BETStyle.h2betTimeMatchStyle,
                        ),
                      ],
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Image.asset(
                            matchH2Bet.secondFootballTeam.teamLogo,
                            height: 50,
                            width: 50,
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      matchH2Bet.firstFootballTeam.teamName,
                      style: H2BETStyle.h2betMatchTeamStyle,
                    ),
                    Text(
                      matchH2Bet.secondFootballTeam.teamName,
                      style: H2BETStyle.h2betMatchTeamStyle,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Center(
          child: Padding(
            padding: const EdgeInsets.only(top: 105),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: InkWell(
                radius: 50,
                splashColor: H2BETColors.h2betTransparentColor,
                onTap: () => context.router.navigate(
                  MatchInfoH2BetRoute(
                    matchH2Bet: matchH2Bet,
                    matchIndex: matchIndex,
                  ),
                ),
                child: Container(
                  color: H2BETColors.matchButtonColor,
                  width: 70,
                  height: 70,
                  child: Center(
                    child: Text(
                      AppLocalizations.of(context)!.h2betMatch,
                      style: H2BETStyle.h2betMatchButtonStyle,
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
