import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:h2bet/app/pages/widgets/menu_h2bet_button.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale.dart';
import 'package:h2bet/core/router/h2bet_router.dart';

@RoutePage()
class MenuH2BETPage extends StatefulWidget {
  const MenuH2BETPage({super.key});

  @override
  State<MenuH2BETPage> createState() => _MenuH2BETPageState();
}

class _MenuH2BETPageState extends State<MenuH2BETPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: PageH2BetWrapper(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MenuH2BetButton(
                buttonName: AppLocalizations.of(context)!.h2betMatches,
                onTap: () => context.router.navigate(const MatchesH2BetRoute()),
              ),
              const SizedBox(
                height: 30,
              ),
              MenuH2BetButton(
                buttonName: AppLocalizations.of(context)!.h2betHistoryTeams,
                onTap: () =>
                    context.router.navigate(const HistoryMenuH2BetRoute()),
              ),
              const SizedBox(
                height: 50,
              ),
              MenuH2BetButton(
                buttonName: AppLocalizations.of(context)!.h2betExit,
                onTap: () => context.router.navigate(const ExitH2BetRoute()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
