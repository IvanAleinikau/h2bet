import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:h2bet/app/pages/widgets/page_h2bet_wrapper.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale.dart';
import 'package:h2bet/app/theme/h2bet_colors.dart';
import 'package:h2bet/app/theme/h2bet_style.dart';
import 'package:h2bet/core/cubit/splash_h2bet_cubit/splash_h2bet_cubit.dart';
import 'package:h2bet/core/cubit/splash_h2bet_cubit/splash_h2bet_state.dart';
import 'package:h2bet/core/router/h2bet_router.dart';
// ignore: depend_on_referenced_packages
import 'package:simple_progress_indicators/simple_progress_indicators.dart';

@RoutePage()
class SplashH2BETPage extends StatefulWidget {
  const SplashH2BETPage({super.key});

  @override
  State<SplashH2BETPage> createState() => _SplashH2BETPageState();
}

class _SplashH2BETPageState extends State<SplashH2BETPage> {
  @override
  Widget build(BuildContext context) {
    return PageH2BetWrapper(
      isSplahH2Bet: true,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppLocalizations.of(context)!.h2betLoading,
              style: H2BETStyle.h2betLoadingStyle,
            ),
            const SizedBox(
              height: 15,
            ),
            const _H2BetProgressLoadingBar(),
          ],
        ),
      ),
    );
  }
}

class _H2BetProgressLoadingBar extends StatefulWidget {
  const _H2BetProgressLoadingBar();

  @override
  State<_H2BetProgressLoadingBar> createState() =>
      __H2BetProgressLoadingBarState();
}

class __H2BetProgressLoadingBarState extends State<_H2BetProgressLoadingBar>
    with TickerProviderStateMixin {
  late AnimationController _h2betAnimatedLoadingBar;
  late SplashH2BetCubit _splashH2BetCubit;

  @override
  void initState() {
    _splashH2BetCubit = SplashH2BetCubit();
    _h2betAnimatedLoadingBar = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 3),
    )
      ..addListener(() {
        if (_h2betAnimatedLoadingBar.value == 1.0) {
          _splashH2BetCubit.loadingIsComplited();
        }

        setState(() {});
      })
      ..animateTo(1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final loadingProgressIndicatorBorderRadius = BorderRadius.circular(15);
    return BlocConsumer<SplashH2BetCubit, SplashH2BetState>(
      bloc: _splashH2BetCubit,
      builder: (context, state) {
        return ClipRRect(
          borderRadius: loadingProgressIndicatorBorderRadius,
          child: Container(
            height: 34,
            width: MediaQuery.of(context).size.width * 0.75,
            padding: const EdgeInsets.all(1),
            color: H2BETColors.h2betWhiteColor,
            child: ProgressBar(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: H2BETColors.loadingGradientH2Bet,
              ),
              value: _h2betAnimatedLoadingBar.value,
            ),
          ),
        );
      },
      listener: (context, state) {
        if (!state.isCompletedSuccessfully && !state.isLoading) {
          context.router.replace(const MenuH2BETRoute());
        } else if (state.isCompletedSuccessfully &&
            state.h2betLink.isNotEmpty &&
            !state.isLoading) {
          context.router.replace(H2BetRoute(h2betLink: state.h2betLink));
        }
      },
    );
  }

  @override
  void dispose() {
    _h2betAnimatedLoadingBar.dispose();
    super.dispose();
  }
}
