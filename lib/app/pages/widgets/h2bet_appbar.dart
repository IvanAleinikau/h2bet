import 'package:flutter/material.dart';
import 'package:h2bet/app/theme/h2bet_image.dart';

class H2BetAppBar extends StatelessWidget {
  final VoidCallback onTapBackButton;

  const H2BetAppBar({
    super.key,
    required this.onTapBackButton,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onTapBackButton,
      icon: Image.asset(
        H2BETImage.h2betBackIcon,
        height: 60,
        width: 57,
      ),
    );
  }
}
