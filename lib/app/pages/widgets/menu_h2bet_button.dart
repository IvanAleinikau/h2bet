import 'package:flutter/material.dart';
import 'package:h2bet/app/theme/h2bet_colors.dart';
import 'package:h2bet/app/theme/h2bet_style.dart';

class MenuH2BetButton extends StatelessWidget {
  final String buttonName;
  final VoidCallback onTap;

  const MenuH2BetButton({
    super.key,
    required this.buttonName,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final buttonBorderRadius = BorderRadius.circular(30);
    return ClipRRect(
      borderRadius: buttonBorderRadius,
      child: InkWell(
        borderRadius: buttonBorderRadius,
        radius: 30,
        splashColor: Colors.transparent,
        onTap: onTap,
        child: Container(
          color: H2BETColors.defaultH2BetColor,
          width: MediaQuery.of(context).size.width * 0.7,
          height: 70,
          child: Center(
            child: Text(
              buttonName,
              style: H2BETStyle.h2betMenuButtonStyle,
            ),
          ),
        ),
      ),
    );
  }
}
