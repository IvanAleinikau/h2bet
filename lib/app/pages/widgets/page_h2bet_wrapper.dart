import 'package:flutter/material.dart';
import 'package:h2bet/app/theme/h2bet_image.dart';

class PageH2BetWrapper extends StatelessWidget {
  final bool isSplahH2Bet;
  final Widget child;

  const PageH2BetWrapper({
    super.key,
    this.isSplahH2Bet = false,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              isSplahH2Bet
                  ? H2BETImage.splashH2BETBackgroundImage
                  : H2BETImage.menuH2BETBackgroundImage,
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: child,
        ),
      ),
    );
  }
}
