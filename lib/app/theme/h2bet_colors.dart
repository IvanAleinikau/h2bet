import 'package:flutter/material.dart';

class H2BETColors {
  H2BETColors._();

  static Color h2betWhiteColor = const Color(0xFFFFFFFF);
  static Color h2betGreenColor = const Color(0xFF4CAF50);
  static Color h2betRedColor = const Color(0xFFF44336);
  static Color h2betYellowColor = const Color(0xFFFFEB3B);
  static Color h2betTransparentColor = Colors.transparent;
  static Color defaultH2BetColor = const Color(0xFFD9D9D9).withOpacity(0.23);
  static Color infoWrapperH2BetColor =
      const Color(0xFF151515).withOpacity(0.65);
  static Color borderGoalMark = const Color(0xFFFFE0E0);
  static Color matchesH2BetColor = const Color(0xFFD9D9D9);
  static Color matchButtonColor = const Color(0xFF5EA8FF);
  static List<Color> loadingGradientH2Bet = [
    Colors.orangeAccent,
    Colors.blueAccent,
  ];
}
