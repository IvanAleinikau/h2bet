class H2BETImage {
  H2BETImage._();

  static const String _imagePathH2BET = 'assets/image';
  static const String splashH2BETBackgroundImage =
      '$_imagePathH2BET/spalsh_background_h2bet_image.png';
  static const String menuH2BETBackgroundImage =
      '$_imagePathH2BET/menu_background_h2bet_image.png';
  static const String h2betBackIcon = '$_imagePathH2BET/h2bet_back_icon.png';

  //footbal teams logo
  static const String napoliLogo = '$_imagePathH2BET/napoli_logo.png';
  static const String unionLogo = '$_imagePathH2BET/union_logo.png';
  static const String realLogo = '$_imagePathH2BET/real_logo.png';
  static const String benficaLogo = '$_imagePathH2BET/benfica_logo.png';
  static const String copenhagenLogo = '$_imagePathH2BET/copenhagen_logo.png';
  static const String manchesterLogo = '$_imagePathH2BET/manchester_logo.png';
  static const String bavariaLogo = '$_imagePathH2BET/bavaria_logo.png';
  static const String galatasarayLogo = '$_imagePathH2BET/galatasaray_logo.png';
  static const String lanceLogo = '$_imagePathH2BET/lance_logo.png';
  static const String psvLogo = '$_imagePathH2BET/psv_logo.png';
  static const String arsenalLogo = '$_imagePathH2BET/arsenal_logo.png';
  static const String salzburgLogo = '$_imagePathH2BET/salzburg_logo.png';
  static const String interLogo = '$_imagePathH2BET/inter_logo.png';
  static const String realMadridLogo = '$_imagePathH2BET/real_madrid_logo.png';
  static const String bragaLogo = '$_imagePathH2BET/braga_logo.png';
  static const String sevilleLogo = '$_imagePathH2BET/seville_logo.png';
}
