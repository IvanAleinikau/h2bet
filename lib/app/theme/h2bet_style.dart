import 'package:flutter/material.dart';

class H2BETStyle {
  H2BETStyle._();

  static const Color _h2betWhite = Color(0xFFFFFFFF);

  static const TextStyle _defaultH2BetTextStyle = TextStyle(
    fontFamily: 'Inter',
    color: _h2betWhite,
    fontWeight: FontWeight.w400,
  );

  static TextStyle h2betLoadingStyle =
      _defaultH2BetTextStyle.copyWith(fontSize: 48);

  static TextStyle h2betMenuButtonStyle =
      _defaultH2BetTextStyle.copyWith(fontSize: 36);

  static TextStyle h2betHistoryTeamStyle =
      _defaultH2BetTextStyle.copyWith(fontSize: 22);

  static TextStyle h2betMatchTeamStyle = _defaultH2BetTextStyle.copyWith(
    fontSize: 20,
    color: const Color(0xFF000000),
  );

  static TextStyle h2betMatchInfoStyle = _defaultH2BetTextStyle.copyWith(
    fontSize: 16,
    color: const Color(0xFFFFFCFC),
  );

  static TextStyle h2betDateMatchStyle = _defaultH2BetTextStyle.copyWith(
      fontSize: 16, color: const Color(0xFF949494));

  static TextStyle h2betTimeMatchStyle = _defaultH2BetTextStyle.copyWith(
    fontSize: 32,
    color: const Color(0xFF000000),
  );

  static TextStyle h2betTimeInfoStyle = _defaultH2BetTextStyle.copyWith(
    fontSize: 32,
  );

  static TextStyle h2betMatchButtonStyle =
      _defaultH2BetTextStyle.copyWith(fontSize: 16);

  static TextStyle h2betMatchInfoSubtitleStyle =
      _defaultH2BetTextStyle.copyWith(
    fontSize: 32,
    color: const Color(0xFFFFFCFC),
  );

  static TextStyle h2betCoeffStyle = _defaultH2BetTextStyle.copyWith(
      fontSize: 16, color: const Color(0xFF1F1A17));

  static TextStyle h2betMatchInfoGoalStyle = _defaultH2BetTextStyle.copyWith(
      fontSize: 24, color: const Color.fromARGB(253, 255, 255, 255));
}
