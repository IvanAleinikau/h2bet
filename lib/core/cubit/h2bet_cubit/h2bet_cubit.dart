import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:h2bet/core/cubit/h2bet_cubit/h2bet_state.dart';
import 'package:permission_handler/permission_handler.dart';
// ignore: depend_on_referenced_packages
import 'package:connectivity_plus/connectivity_plus.dart';

class H2BetCubit extends Cubit<H2BetState> {
  H2BetCubit() : super(const H2BetState()) {
    onInit();
  }

  final Connectivity _connectivityH2Bet = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscriptionH2Bet;

  void onInit() async {
    await requestPermission();
    _connectivitySubscriptionH2Bet = _connectivityH2Bet.onConnectivityChanged
        .listen(_updateConnectionStatusH2Bet);
  }

  Future<void> _updateConnectionStatusH2Bet(ConnectivityResult result) async {
    if (result != ConnectivityResult.ethernet &&
        result != ConnectivityResult.wifi &&
        result != ConnectivityResult.mobile) {
      emit(state.copyWith(isHavaInternet: false));
      disposeH2Bet();
    }
  }

  void saveH2BetCookie(String cookie) {
    emit(state.copyWith(cookie: cookie));
  }

  void disposeH2Bet() {
    _connectivitySubscriptionH2Bet.cancel();
  }

  void updateH2BetLink(String h2betLink) {
    emit(state.copyWith(h2betUrl: h2betLink));
  }

  void finishLoad() {
    emit(state.copyWith(isLoading: false));
  }

  Future<void> requestPermission() async {
    await [
      Permission.camera,
      Permission.mediaLibrary,
      Permission.bluetooth,
    ].request();
  }
}
