import 'package:freezed_annotation/freezed_annotation.dart';

part 'h2bet_state.freezed.dart';

@freezed
class H2BetState with _$H2BetState {
  const factory H2BetState({
    @Default(true) bool isLoading,
    @Default(true) bool isHavaInternet,
    @Default('') String h2betUrl,
    @Default('') String cookie,
  }) = _H2BetState;
}
