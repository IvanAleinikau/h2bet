// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'h2bet_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$H2BetState {
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isHavaInternet => throw _privateConstructorUsedError;
  String get h2betUrl => throw _privateConstructorUsedError;
  String get cookie => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $H2BetStateCopyWith<H2BetState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $H2BetStateCopyWith<$Res> {
  factory $H2BetStateCopyWith(
          H2BetState value, $Res Function(H2BetState) then) =
      _$H2BetStateCopyWithImpl<$Res, H2BetState>;
  @useResult
  $Res call(
      {bool isLoading, bool isHavaInternet, String h2betUrl, String cookie});
}

/// @nodoc
class _$H2BetStateCopyWithImpl<$Res, $Val extends H2BetState>
    implements $H2BetStateCopyWith<$Res> {
  _$H2BetStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isHavaInternet = null,
    Object? h2betUrl = null,
    Object? cookie = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isHavaInternet: null == isHavaInternet
          ? _value.isHavaInternet
          : isHavaInternet // ignore: cast_nullable_to_non_nullable
              as bool,
      h2betUrl: null == h2betUrl
          ? _value.h2betUrl
          : h2betUrl // ignore: cast_nullable_to_non_nullable
              as String,
      cookie: null == cookie
          ? _value.cookie
          : cookie // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$H2BetStateImplCopyWith<$Res>
    implements $H2BetStateCopyWith<$Res> {
  factory _$$H2BetStateImplCopyWith(
          _$H2BetStateImpl value, $Res Function(_$H2BetStateImpl) then) =
      __$$H2BetStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isLoading, bool isHavaInternet, String h2betUrl, String cookie});
}

/// @nodoc
class __$$H2BetStateImplCopyWithImpl<$Res>
    extends _$H2BetStateCopyWithImpl<$Res, _$H2BetStateImpl>
    implements _$$H2BetStateImplCopyWith<$Res> {
  __$$H2BetStateImplCopyWithImpl(
      _$H2BetStateImpl _value, $Res Function(_$H2BetStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isHavaInternet = null,
    Object? h2betUrl = null,
    Object? cookie = null,
  }) {
    return _then(_$H2BetStateImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isHavaInternet: null == isHavaInternet
          ? _value.isHavaInternet
          : isHavaInternet // ignore: cast_nullable_to_non_nullable
              as bool,
      h2betUrl: null == h2betUrl
          ? _value.h2betUrl
          : h2betUrl // ignore: cast_nullable_to_non_nullable
              as String,
      cookie: null == cookie
          ? _value.cookie
          : cookie // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$H2BetStateImpl implements _H2BetState {
  const _$H2BetStateImpl(
      {this.isLoading = true,
      this.isHavaInternet = true,
      this.h2betUrl = '',
      this.cookie = ''});

  @override
  @JsonKey()
  final bool isLoading;
  @override
  @JsonKey()
  final bool isHavaInternet;
  @override
  @JsonKey()
  final String h2betUrl;
  @override
  @JsonKey()
  final String cookie;

  @override
  String toString() {
    return 'H2BetState(isLoading: $isLoading, isHavaInternet: $isHavaInternet, h2betUrl: $h2betUrl, cookie: $cookie)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$H2BetStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isHavaInternet, isHavaInternet) ||
                other.isHavaInternet == isHavaInternet) &&
            (identical(other.h2betUrl, h2betUrl) ||
                other.h2betUrl == h2betUrl) &&
            (identical(other.cookie, cookie) || other.cookie == cookie));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isLoading, isHavaInternet, h2betUrl, cookie);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$H2BetStateImplCopyWith<_$H2BetStateImpl> get copyWith =>
      __$$H2BetStateImplCopyWithImpl<_$H2BetStateImpl>(this, _$identity);
}

abstract class _H2BetState implements H2BetState {
  const factory _H2BetState(
      {final bool isLoading,
      final bool isHavaInternet,
      final String h2betUrl,
      final String cookie}) = _$H2BetStateImpl;

  @override
  bool get isLoading;
  @override
  bool get isHavaInternet;
  @override
  String get h2betUrl;
  @override
  String get cookie;
  @override
  @JsonKey(ignore: true)
  _$$H2BetStateImplCopyWith<_$H2BetStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
