import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:h2bet/core/cubit/history_football_teams_cubit/history_teams_state.dart';
import 'package:h2bet/data/football_team_h2bet_repo.dart';

class HistoryTeamsCubit extends Cubit<HistoryTeamsState> {
  HistoryTeamsCubit() : super(const HistoryTeamsState()) {
    onInit();
  }

  final _footballTeamRepo = GetIt.instance<FootballTeamH2BetRepository>();

  void onInit() {
    final footballTeams =
        _footballTeamRepo.footballTeamDatabase.values.toList(growable: false);
    emit(state.copyWith(footballTeamsH2Bet: footballTeams));
  }
}
