import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:h2bet/core/model/football_team_h2bet.dart';

part 'history_teams_state.freezed.dart';

@freezed
class HistoryTeamsState with _$HistoryTeamsState {
  const factory HistoryTeamsState({
    @Default(false) bool isLoading,
    @Default([]) List<FootballTeamH2Bet> footballTeamsH2Bet,
  }) = _HistoryTeamsState;
}
