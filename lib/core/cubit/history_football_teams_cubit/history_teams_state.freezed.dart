// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'history_teams_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HistoryTeamsState {
  bool get isLoading => throw _privateConstructorUsedError;
  List<FootballTeamH2Bet> get footballTeamsH2Bet =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HistoryTeamsStateCopyWith<HistoryTeamsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HistoryTeamsStateCopyWith<$Res> {
  factory $HistoryTeamsStateCopyWith(
          HistoryTeamsState value, $Res Function(HistoryTeamsState) then) =
      _$HistoryTeamsStateCopyWithImpl<$Res, HistoryTeamsState>;
  @useResult
  $Res call({bool isLoading, List<FootballTeamH2Bet> footballTeamsH2Bet});
}

/// @nodoc
class _$HistoryTeamsStateCopyWithImpl<$Res, $Val extends HistoryTeamsState>
    implements $HistoryTeamsStateCopyWith<$Res> {
  _$HistoryTeamsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? footballTeamsH2Bet = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      footballTeamsH2Bet: null == footballTeamsH2Bet
          ? _value.footballTeamsH2Bet
          : footballTeamsH2Bet // ignore: cast_nullable_to_non_nullable
              as List<FootballTeamH2Bet>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HistoryTeamsStateImplCopyWith<$Res>
    implements $HistoryTeamsStateCopyWith<$Res> {
  factory _$$HistoryTeamsStateImplCopyWith(_$HistoryTeamsStateImpl value,
          $Res Function(_$HistoryTeamsStateImpl) then) =
      __$$HistoryTeamsStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isLoading, List<FootballTeamH2Bet> footballTeamsH2Bet});
}

/// @nodoc
class __$$HistoryTeamsStateImplCopyWithImpl<$Res>
    extends _$HistoryTeamsStateCopyWithImpl<$Res, _$HistoryTeamsStateImpl>
    implements _$$HistoryTeamsStateImplCopyWith<$Res> {
  __$$HistoryTeamsStateImplCopyWithImpl(_$HistoryTeamsStateImpl _value,
      $Res Function(_$HistoryTeamsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? footballTeamsH2Bet = null,
  }) {
    return _then(_$HistoryTeamsStateImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      footballTeamsH2Bet: null == footballTeamsH2Bet
          ? _value._footballTeamsH2Bet
          : footballTeamsH2Bet // ignore: cast_nullable_to_non_nullable
              as List<FootballTeamH2Bet>,
    ));
  }
}

/// @nodoc

class _$HistoryTeamsStateImpl implements _HistoryTeamsState {
  const _$HistoryTeamsStateImpl(
      {this.isLoading = false,
      final List<FootballTeamH2Bet> footballTeamsH2Bet = const []})
      : _footballTeamsH2Bet = footballTeamsH2Bet;

  @override
  @JsonKey()
  final bool isLoading;
  final List<FootballTeamH2Bet> _footballTeamsH2Bet;
  @override
  @JsonKey()
  List<FootballTeamH2Bet> get footballTeamsH2Bet {
    if (_footballTeamsH2Bet is EqualUnmodifiableListView)
      return _footballTeamsH2Bet;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_footballTeamsH2Bet);
  }

  @override
  String toString() {
    return 'HistoryTeamsState(isLoading: $isLoading, footballTeamsH2Bet: $footballTeamsH2Bet)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HistoryTeamsStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            const DeepCollectionEquality()
                .equals(other._footballTeamsH2Bet, _footballTeamsH2Bet));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLoading,
      const DeepCollectionEquality().hash(_footballTeamsH2Bet));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HistoryTeamsStateImplCopyWith<_$HistoryTeamsStateImpl> get copyWith =>
      __$$HistoryTeamsStateImplCopyWithImpl<_$HistoryTeamsStateImpl>(
          this, _$identity);
}

abstract class _HistoryTeamsState implements HistoryTeamsState {
  const factory _HistoryTeamsState(
          {final bool isLoading,
          final List<FootballTeamH2Bet> footballTeamsH2Bet}) =
      _$HistoryTeamsStateImpl;

  @override
  bool get isLoading;
  @override
  List<FootballTeamH2Bet> get footballTeamsH2Bet;
  @override
  @JsonKey(ignore: true)
  _$$HistoryTeamsStateImplCopyWith<_$HistoryTeamsStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
