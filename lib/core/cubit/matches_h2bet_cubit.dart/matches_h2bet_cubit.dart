import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:h2bet/core/cubit/matches_h2bet_cubit.dart/matches_h2bet_state.dart';
import 'package:h2bet/data/matches_h2bet_repo.dart';

class MatchesH2BetCubit extends Cubit<MatchesH2BetState> {
  MatchesH2BetCubit() : super(const MatchesH2BetState()) {
    onInit();
  }

  final _matchesH2BetRepo = GetIt.instance<MatchesH2BetRepository>();

  void onInit() {
    emit(state.copyWith(matchesH2Bet: _matchesH2BetRepo.matchesH2Bet));
  }
}
