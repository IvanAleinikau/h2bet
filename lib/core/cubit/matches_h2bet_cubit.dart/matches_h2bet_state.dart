import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:h2bet/core/model/match_h2bet.dart';

part 'matches_h2bet_state.freezed.dart';

@freezed
class MatchesH2BetState with _$MatchesH2BetState {
  const factory MatchesH2BetState({
    @Default(false) bool isLoading,
    @Default([]) List<MatchH2Bet> matchesH2Bet,
  }) = _MatchesH2BetState;
}
