// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'matches_h2bet_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MatchesH2BetState {
  bool get isLoading => throw _privateConstructorUsedError;
  List<MatchH2Bet> get matchesH2Bet => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MatchesH2BetStateCopyWith<MatchesH2BetState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MatchesH2BetStateCopyWith<$Res> {
  factory $MatchesH2BetStateCopyWith(
          MatchesH2BetState value, $Res Function(MatchesH2BetState) then) =
      _$MatchesH2BetStateCopyWithImpl<$Res, MatchesH2BetState>;
  @useResult
  $Res call({bool isLoading, List<MatchH2Bet> matchesH2Bet});
}

/// @nodoc
class _$MatchesH2BetStateCopyWithImpl<$Res, $Val extends MatchesH2BetState>
    implements $MatchesH2BetStateCopyWith<$Res> {
  _$MatchesH2BetStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? matchesH2Bet = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      matchesH2Bet: null == matchesH2Bet
          ? _value.matchesH2Bet
          : matchesH2Bet // ignore: cast_nullable_to_non_nullable
              as List<MatchH2Bet>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MatchesH2BetStateImplCopyWith<$Res>
    implements $MatchesH2BetStateCopyWith<$Res> {
  factory _$$MatchesH2BetStateImplCopyWith(_$MatchesH2BetStateImpl value,
          $Res Function(_$MatchesH2BetStateImpl) then) =
      __$$MatchesH2BetStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isLoading, List<MatchH2Bet> matchesH2Bet});
}

/// @nodoc
class __$$MatchesH2BetStateImplCopyWithImpl<$Res>
    extends _$MatchesH2BetStateCopyWithImpl<$Res, _$MatchesH2BetStateImpl>
    implements _$$MatchesH2BetStateImplCopyWith<$Res> {
  __$$MatchesH2BetStateImplCopyWithImpl(_$MatchesH2BetStateImpl _value,
      $Res Function(_$MatchesH2BetStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? matchesH2Bet = null,
  }) {
    return _then(_$MatchesH2BetStateImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      matchesH2Bet: null == matchesH2Bet
          ? _value._matchesH2Bet
          : matchesH2Bet // ignore: cast_nullable_to_non_nullable
              as List<MatchH2Bet>,
    ));
  }
}

/// @nodoc

class _$MatchesH2BetStateImpl implements _MatchesH2BetState {
  const _$MatchesH2BetStateImpl(
      {this.isLoading = false, final List<MatchH2Bet> matchesH2Bet = const []})
      : _matchesH2Bet = matchesH2Bet;

  @override
  @JsonKey()
  final bool isLoading;
  final List<MatchH2Bet> _matchesH2Bet;
  @override
  @JsonKey()
  List<MatchH2Bet> get matchesH2Bet {
    if (_matchesH2Bet is EqualUnmodifiableListView) return _matchesH2Bet;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_matchesH2Bet);
  }

  @override
  String toString() {
    return 'MatchesH2BetState(isLoading: $isLoading, matchesH2Bet: $matchesH2Bet)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MatchesH2BetStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            const DeepCollectionEquality()
                .equals(other._matchesH2Bet, _matchesH2Bet));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLoading,
      const DeepCollectionEquality().hash(_matchesH2Bet));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MatchesH2BetStateImplCopyWith<_$MatchesH2BetStateImpl> get copyWith =>
      __$$MatchesH2BetStateImplCopyWithImpl<_$MatchesH2BetStateImpl>(
          this, _$identity);
}

abstract class _MatchesH2BetState implements MatchesH2BetState {
  const factory _MatchesH2BetState(
      {final bool isLoading,
      final List<MatchH2Bet> matchesH2Bet}) = _$MatchesH2BetStateImpl;

  @override
  bool get isLoading;
  @override
  List<MatchH2Bet> get matchesH2Bet;
  @override
  @JsonKey(ignore: true)
  _$$MatchesH2BetStateImplCopyWith<_$MatchesH2BetStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
