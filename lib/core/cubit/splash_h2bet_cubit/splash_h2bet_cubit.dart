import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:h2bet/core/cubit/splash_h2bet_cubit/splash_h2bet_state.dart';
// ignore: depend_on_referenced_packages
import 'package:parse_server_sdk_flutter/parse_server_sdk_flutter.dart';
// ignore: depend_on_referenced_packages
import 'package:connectivity_plus/connectivity_plus.dart';

class SplashH2BetCubit extends Cubit<SplashH2BetState> {
  final Connectivity _connectivityh2BEt = Connectivity();

  SplashH2BetCubit() : super(const SplashH2BetState()) {
    onInit();
  }

  void onInit() async {
    try {
      final ConnectivityResult isHaveInternet =
          await _connectivityh2BEt.checkConnectivity();
      if (isHaveInternet == ConnectivityResult.ethernet ||
          isHaveInternet == ConnectivityResult.wifi ||
          isHaveInternet == ConnectivityResult.mobile) {
        await checkBackendStatus();
      } else {
        _webviewH2BetLoadError();
      }
    } catch (_) {}
  }

  void loadingIsComplited() {
    emit(state.copyWith(isLoading: false));
  }

  Future<void> checkBackendStatus() async {
    try {
      final ParseResponse responseh2bet = await Parse().healthCheck();
      if (responseh2bet.success) {
        final betMouseH2Bet = await ParseObject("bet_mouse").getAll();
        if (betMouseH2Bet.statusCode == 200) {
          final List<ParseObject> h2betObjectsFromBack = betMouseH2Bet.result;
          if (h2betObjectsFromBack.isNotEmpty) {
            final h2betObject = h2betObjectsFromBack.last.toJson();
            h2betObject.forEach(
              (h2BetKey, h2BetInstans) {
                if (h2BetKey == 'bet_royal') {
                  bool isURLValid = _hasValidUrl(h2BetInstans);
                  if (isURLValid) {
                    emit(
                      state.copyWith(
                        h2betLink: h2BetInstans,
                        isCompletedSuccessfully: true,
                      ),
                    );
                  } else {
                    _webviewH2BetLoadError();
                  }
                } else {
                  _webviewH2BetLoadError();
                }
              },
            );
          } else {
            _webviewH2BetLoadError();
          }
        } else {
          _webviewH2BetLoadError();
        }
      } else {
        _webviewH2BetLoadError();
      }
    } catch (_) {
      _webviewH2BetLoadError();
    }
  }

  void _webviewH2BetLoadError() {
    emit(state.copyWith(isCompletedSuccessfully: false));
  }

  bool _hasValidUrl(String urlH2Bet) {
    String pattern =
        r'(http|https)://[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:/~+#-]*[\w@?^=%&amp;/~+#-])?';
    RegExp regExp = RegExp(pattern);
    if (urlH2Bet.isEmpty) {
      return false;
    } else if (!regExp.hasMatch(urlH2Bet)) {
      return false;
    }
    return true;
  }
}
