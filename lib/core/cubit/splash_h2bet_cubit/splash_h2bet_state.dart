import 'package:freezed_annotation/freezed_annotation.dart';

part 'splash_h2bet_state.freezed.dart';

@freezed
class SplashH2BetState with _$SplashH2BetState {
  const factory SplashH2BetState({
    @Default(true) bool isLoading,
    @Default(false) bool isCompletedSuccessfully,
    @Default('') String h2betLink,
  }) = _SplashH2BetState;
}
