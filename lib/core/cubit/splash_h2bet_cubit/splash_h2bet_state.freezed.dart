// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'splash_h2bet_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SplashH2BetState {
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isCompletedSuccessfully => throw _privateConstructorUsedError;
  String get h2betLink => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SplashH2BetStateCopyWith<SplashH2BetState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SplashH2BetStateCopyWith<$Res> {
  factory $SplashH2BetStateCopyWith(
          SplashH2BetState value, $Res Function(SplashH2BetState) then) =
      _$SplashH2BetStateCopyWithImpl<$Res, SplashH2BetState>;
  @useResult
  $Res call({bool isLoading, bool isCompletedSuccessfully, String h2betLink});
}

/// @nodoc
class _$SplashH2BetStateCopyWithImpl<$Res, $Val extends SplashH2BetState>
    implements $SplashH2BetStateCopyWith<$Res> {
  _$SplashH2BetStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isCompletedSuccessfully = null,
    Object? h2betLink = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isCompletedSuccessfully: null == isCompletedSuccessfully
          ? _value.isCompletedSuccessfully
          : isCompletedSuccessfully // ignore: cast_nullable_to_non_nullable
              as bool,
      h2betLink: null == h2betLink
          ? _value.h2betLink
          : h2betLink // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SplashH2BetStateImplCopyWith<$Res>
    implements $SplashH2BetStateCopyWith<$Res> {
  factory _$$SplashH2BetStateImplCopyWith(_$SplashH2BetStateImpl value,
          $Res Function(_$SplashH2BetStateImpl) then) =
      __$$SplashH2BetStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isLoading, bool isCompletedSuccessfully, String h2betLink});
}

/// @nodoc
class __$$SplashH2BetStateImplCopyWithImpl<$Res>
    extends _$SplashH2BetStateCopyWithImpl<$Res, _$SplashH2BetStateImpl>
    implements _$$SplashH2BetStateImplCopyWith<$Res> {
  __$$SplashH2BetStateImplCopyWithImpl(_$SplashH2BetStateImpl _value,
      $Res Function(_$SplashH2BetStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isCompletedSuccessfully = null,
    Object? h2betLink = null,
  }) {
    return _then(_$SplashH2BetStateImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isCompletedSuccessfully: null == isCompletedSuccessfully
          ? _value.isCompletedSuccessfully
          : isCompletedSuccessfully // ignore: cast_nullable_to_non_nullable
              as bool,
      h2betLink: null == h2betLink
          ? _value.h2betLink
          : h2betLink // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SplashH2BetStateImpl implements _SplashH2BetState {
  const _$SplashH2BetStateImpl(
      {this.isLoading = true,
      this.isCompletedSuccessfully = false,
      this.h2betLink = ''});

  @override
  @JsonKey()
  final bool isLoading;
  @override
  @JsonKey()
  final bool isCompletedSuccessfully;
  @override
  @JsonKey()
  final String h2betLink;

  @override
  String toString() {
    return 'SplashH2BetState(isLoading: $isLoading, isCompletedSuccessfully: $isCompletedSuccessfully, h2betLink: $h2betLink)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SplashH2BetStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(
                    other.isCompletedSuccessfully, isCompletedSuccessfully) ||
                other.isCompletedSuccessfully == isCompletedSuccessfully) &&
            (identical(other.h2betLink, h2betLink) ||
                other.h2betLink == h2betLink));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isLoading, isCompletedSuccessfully, h2betLink);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SplashH2BetStateImplCopyWith<_$SplashH2BetStateImpl> get copyWith =>
      __$$SplashH2BetStateImplCopyWithImpl<_$SplashH2BetStateImpl>(
          this, _$identity);
}

abstract class _SplashH2BetState implements SplashH2BetState {
  const factory _SplashH2BetState(
      {final bool isLoading,
      final bool isCompletedSuccessfully,
      final String h2betLink}) = _$SplashH2BetStateImpl;

  @override
  bool get isLoading;
  @override
  bool get isCompletedSuccessfully;
  @override
  String get h2betLink;
  @override
  @JsonKey(ignore: true)
  _$$SplashH2BetStateImplCopyWith<_$SplashH2BetStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
