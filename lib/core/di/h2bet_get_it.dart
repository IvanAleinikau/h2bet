import 'package:get_it/get_it.dart';
import 'package:h2bet/data/football_team_h2bet_repo.dart';
import 'package:h2bet/data/matches_h2bet_repo.dart';

class H2BetGetIt {
  static setupGetIt() {
    GetIt.instance.registerSingleton(FootballTeamH2BetRepository());
    GetIt.instance.registerSingleton(MatchesH2BetRepository());
  }
}
