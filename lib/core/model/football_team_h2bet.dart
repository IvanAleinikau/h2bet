import 'package:freezed_annotation/freezed_annotation.dart';

part 'football_team_h2bet.freezed.dart';

@freezed
class FootballTeamH2Bet with _$FootballTeamH2Bet {
  factory FootballTeamH2Bet({
    required String teamName,
    required String teamLogo,
    required String teamHistory,
  }) = _FootballTeamH2Bets;
}

final FootballTeamH2Bet emptyFootballTeam = FootballTeamH2Bet(
  teamName: '',
  teamLogo: '',
  teamHistory: '',
);
