// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'football_team_h2bet.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FootballTeamH2Bet {
  String get teamName => throw _privateConstructorUsedError;
  String get teamLogo => throw _privateConstructorUsedError;
  String get teamHistory => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FootballTeamH2BetCopyWith<FootballTeamH2Bet> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FootballTeamH2BetCopyWith<$Res> {
  factory $FootballTeamH2BetCopyWith(
          FootballTeamH2Bet value, $Res Function(FootballTeamH2Bet) then) =
      _$FootballTeamH2BetCopyWithImpl<$Res, FootballTeamH2Bet>;
  @useResult
  $Res call({String teamName, String teamLogo, String teamHistory});
}

/// @nodoc
class _$FootballTeamH2BetCopyWithImpl<$Res, $Val extends FootballTeamH2Bet>
    implements $FootballTeamH2BetCopyWith<$Res> {
  _$FootballTeamH2BetCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? teamName = null,
    Object? teamLogo = null,
    Object? teamHistory = null,
  }) {
    return _then(_value.copyWith(
      teamName: null == teamName
          ? _value.teamName
          : teamName // ignore: cast_nullable_to_non_nullable
              as String,
      teamLogo: null == teamLogo
          ? _value.teamLogo
          : teamLogo // ignore: cast_nullable_to_non_nullable
              as String,
      teamHistory: null == teamHistory
          ? _value.teamHistory
          : teamHistory // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FootballTeamH2BetsImplCopyWith<$Res>
    implements $FootballTeamH2BetCopyWith<$Res> {
  factory _$$FootballTeamH2BetsImplCopyWith(_$FootballTeamH2BetsImpl value,
          $Res Function(_$FootballTeamH2BetsImpl) then) =
      __$$FootballTeamH2BetsImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String teamName, String teamLogo, String teamHistory});
}

/// @nodoc
class __$$FootballTeamH2BetsImplCopyWithImpl<$Res>
    extends _$FootballTeamH2BetCopyWithImpl<$Res, _$FootballTeamH2BetsImpl>
    implements _$$FootballTeamH2BetsImplCopyWith<$Res> {
  __$$FootballTeamH2BetsImplCopyWithImpl(_$FootballTeamH2BetsImpl _value,
      $Res Function(_$FootballTeamH2BetsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? teamName = null,
    Object? teamLogo = null,
    Object? teamHistory = null,
  }) {
    return _then(_$FootballTeamH2BetsImpl(
      teamName: null == teamName
          ? _value.teamName
          : teamName // ignore: cast_nullable_to_non_nullable
              as String,
      teamLogo: null == teamLogo
          ? _value.teamLogo
          : teamLogo // ignore: cast_nullable_to_non_nullable
              as String,
      teamHistory: null == teamHistory
          ? _value.teamHistory
          : teamHistory // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FootballTeamH2BetsImpl implements _FootballTeamH2Bets {
  _$FootballTeamH2BetsImpl(
      {required this.teamName,
      required this.teamLogo,
      required this.teamHistory});

  @override
  final String teamName;
  @override
  final String teamLogo;
  @override
  final String teamHistory;

  @override
  String toString() {
    return 'FootballTeamH2Bet(teamName: $teamName, teamLogo: $teamLogo, teamHistory: $teamHistory)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FootballTeamH2BetsImpl &&
            (identical(other.teamName, teamName) ||
                other.teamName == teamName) &&
            (identical(other.teamLogo, teamLogo) ||
                other.teamLogo == teamLogo) &&
            (identical(other.teamHistory, teamHistory) ||
                other.teamHistory == teamHistory));
  }

  @override
  int get hashCode => Object.hash(runtimeType, teamName, teamLogo, teamHistory);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FootballTeamH2BetsImplCopyWith<_$FootballTeamH2BetsImpl> get copyWith =>
      __$$FootballTeamH2BetsImplCopyWithImpl<_$FootballTeamH2BetsImpl>(
          this, _$identity);
}

abstract class _FootballTeamH2Bets implements FootballTeamH2Bet {
  factory _FootballTeamH2Bets(
      {required final String teamName,
      required final String teamLogo,
      required final String teamHistory}) = _$FootballTeamH2BetsImpl;

  @override
  String get teamName;
  @override
  String get teamLogo;
  @override
  String get teamHistory;
  @override
  @JsonKey(ignore: true)
  _$$FootballTeamH2BetsImplCopyWith<_$FootballTeamH2BetsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
