import 'package:h2bet/core/model/football_team_h2bet.dart';

class MatchH2Bet {
  final FootballTeamH2Bet firstFootballTeam;
  final FootballTeamH2Bet secondFootballTeam;
  final DateTime matchDateTime;
  final double firstTeamCoefficient;
  final double secondTeamCoefficient;
  final String matchPreViewInformation;
  final List<int> goalInfoFirstTeam;
  final List<int> goalInfoSecondTeam;

  MatchH2Bet({
    required this.firstFootballTeam,
    required this.secondFootballTeam,
    required this.matchDateTime,
    required this.firstTeamCoefficient,
    required this.secondTeamCoefficient,
    required this.matchPreViewInformation,
    required this.goalInfoFirstTeam,
    required this.goalInfoSecondTeam,
  });
}
