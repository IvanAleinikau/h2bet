import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:h2bet/app/pages/exit_h2bet.dart';
import 'package:h2bet/app/pages/h2bet_page.dart';
import 'package:h2bet/app/pages/history_menu_h2bet_page.dart';
import 'package:h2bet/app/pages/history_team_h2bet_page.dart';
import 'package:h2bet/app/pages/match_info_h2bet.dart';
import 'package:h2bet/app/pages/matches_h2bet_page.dart';
import 'package:h2bet/app/pages/menu_h2bet_page.dart';
import 'package:h2bet/app/pages/splash_h2bet_page.dart';
import 'package:h2bet/core/model/football_team_h2bet.dart';
import 'package:h2bet/core/model/match_h2bet.dart';

part 'h2bet_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class H2BetRouter extends _$H2BetRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: '/splashH2bet',
          page: SplashH2BETRoute.page,
          initial: true,
        ),
        AutoRoute(
          path: '/menuH2bet',
          page: MenuH2BETRoute.page,
        ),
        AutoRoute(
          path: '/historyTeams',
          page: HistoryMenuH2BetRoute.page,
        ),
        AutoRoute(
          path: '/historyTeamInfo',
          page: HistoryTeamH2BetRoute.page,
        ),
        AutoRoute(
          path: '/exitH2BEt',
          page: ExitH2BetRoute.page,
        ),
        AutoRoute(
          path: '/matchesH2Bet',
          page: MatchesH2BetRoute.page,
        ),
        AutoRoute(
          path: '/matchInfoH2Bet',
          page: MatchInfoH2BetRoute.page,
        ),
        AutoRoute(
          path: '/h2bet',
          page: H2BetRoute.page,
        ),
      ];
}
