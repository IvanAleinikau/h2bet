// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'h2bet_router.dart';

abstract class _$H2BetRouter extends RootStackRouter {
  // ignore: unused_element
  _$H2BetRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    ExitH2BetRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ExitH2BetPage(),
      );
    },
    H2BetRoute.name: (routeData) {
      final args = routeData.argsAs<H2BetRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: H2BetPage(
          key: args.key,
          h2betLink: args.h2betLink,
        ),
      );
    },
    HistoryMenuH2BetRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HistoryMenuH2BetPage(),
      );
    },
    HistoryTeamH2BetRoute.name: (routeData) {
      final args = routeData.argsAs<HistoryTeamH2BetRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: HistoryTeamH2BetPage(
          key: args.key,
          footballTeamH2Bet: args.footballTeamH2Bet,
        ),
      );
    },
    MatchInfoH2BetRoute.name: (routeData) {
      final args = routeData.argsAs<MatchInfoH2BetRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: MatchInfoH2BetPage(
          key: args.key,
          matchH2Bet: args.matchH2Bet,
          matchIndex: args.matchIndex,
        ),
      );
    },
    MatchesH2BetRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MatchesH2BetPage(),
      );
    },
    MenuH2BETRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MenuH2BETPage(),
      );
    },
    SplashH2BETRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SplashH2BETPage(),
      );
    },
  };
}

/// generated route for
/// [ExitH2BetPage]
class ExitH2BetRoute extends PageRouteInfo<void> {
  const ExitH2BetRoute({List<PageRouteInfo>? children})
      : super(
          ExitH2BetRoute.name,
          initialChildren: children,
        );

  static const String name = 'ExitH2BetRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [H2BetPage]
class H2BetRoute extends PageRouteInfo<H2BetRouteArgs> {
  H2BetRoute({
    Key? key,
    required String h2betLink,
    List<PageRouteInfo>? children,
  }) : super(
          H2BetRoute.name,
          args: H2BetRouteArgs(
            key: key,
            h2betLink: h2betLink,
          ),
          initialChildren: children,
        );

  static const String name = 'H2BetRoute';

  static const PageInfo<H2BetRouteArgs> page = PageInfo<H2BetRouteArgs>(name);
}

class H2BetRouteArgs {
  const H2BetRouteArgs({
    this.key,
    required this.h2betLink,
  });

  final Key? key;

  final String h2betLink;

  @override
  String toString() {
    return 'H2BetRouteArgs{key: $key, h2betLink: $h2betLink}';
  }
}

/// generated route for
/// [HistoryMenuH2BetPage]
class HistoryMenuH2BetRoute extends PageRouteInfo<void> {
  const HistoryMenuH2BetRoute({List<PageRouteInfo>? children})
      : super(
          HistoryMenuH2BetRoute.name,
          initialChildren: children,
        );

  static const String name = 'HistoryMenuH2BetRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [HistoryTeamH2BetPage]
class HistoryTeamH2BetRoute extends PageRouteInfo<HistoryTeamH2BetRouteArgs> {
  HistoryTeamH2BetRoute({
    Key? key,
    required FootballTeamH2Bet footballTeamH2Bet,
    List<PageRouteInfo>? children,
  }) : super(
          HistoryTeamH2BetRoute.name,
          args: HistoryTeamH2BetRouteArgs(
            key: key,
            footballTeamH2Bet: footballTeamH2Bet,
          ),
          initialChildren: children,
        );

  static const String name = 'HistoryTeamH2BetRoute';

  static const PageInfo<HistoryTeamH2BetRouteArgs> page =
      PageInfo<HistoryTeamH2BetRouteArgs>(name);
}

class HistoryTeamH2BetRouteArgs {
  const HistoryTeamH2BetRouteArgs({
    this.key,
    required this.footballTeamH2Bet,
  });

  final Key? key;

  final FootballTeamH2Bet footballTeamH2Bet;

  @override
  String toString() {
    return 'HistoryTeamH2BetRouteArgs{key: $key, footballTeamH2Bet: $footballTeamH2Bet}';
  }
}

/// generated route for
/// [MatchInfoH2BetPage]
class MatchInfoH2BetRoute extends PageRouteInfo<MatchInfoH2BetRouteArgs> {
  MatchInfoH2BetRoute({
    Key? key,
    required MatchH2Bet matchH2Bet,
    required int matchIndex,
    List<PageRouteInfo>? children,
  }) : super(
          MatchInfoH2BetRoute.name,
          args: MatchInfoH2BetRouteArgs(
            key: key,
            matchH2Bet: matchH2Bet,
            matchIndex: matchIndex,
          ),
          initialChildren: children,
        );

  static const String name = 'MatchInfoH2BetRoute';

  static const PageInfo<MatchInfoH2BetRouteArgs> page =
      PageInfo<MatchInfoH2BetRouteArgs>(name);
}

class MatchInfoH2BetRouteArgs {
  const MatchInfoH2BetRouteArgs({
    this.key,
    required this.matchH2Bet,
    required this.matchIndex,
  });

  final Key? key;

  final MatchH2Bet matchH2Bet;

  final int matchIndex;

  @override
  String toString() {
    return 'MatchInfoH2BetRouteArgs{key: $key, matchH2Bet: $matchH2Bet, matchIndex: $matchIndex}';
  }
}

/// generated route for
/// [MatchesH2BetPage]
class MatchesH2BetRoute extends PageRouteInfo<void> {
  const MatchesH2BetRoute({List<PageRouteInfo>? children})
      : super(
          MatchesH2BetRoute.name,
          initialChildren: children,
        );

  static const String name = 'MatchesH2BetRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [MenuH2BETPage]
class MenuH2BETRoute extends PageRouteInfo<void> {
  const MenuH2BETRoute({List<PageRouteInfo>? children})
      : super(
          MenuH2BETRoute.name,
          initialChildren: children,
        );

  static const String name = 'MenuH2BETRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SplashH2BETPage]
class SplashH2BETRoute extends PageRouteInfo<void> {
  const SplashH2BETRoute({List<PageRouteInfo>? children})
      : super(
          SplashH2BETRoute.name,
          initialChildren: children,
        );

  static const String name = 'SplashH2BETRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
