import 'package:h2bet/app/theme/h2bet_image.dart';
import 'package:h2bet/core/model/football_team_h2bet.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale_en.dart';

class FootballTeamH2BetRepository {
  Map<String, FootballTeamH2Bet> get footballTeamDatabase => {
        'Napoli': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betNapoli,
          teamLogo: H2BETImage.napoliLogo,
          teamHistory: AppLocalizationsEn().h2betNapoliHistory,
        ),
        'Union': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betUnion,
          teamLogo: H2BETImage.unionLogo,
          teamHistory: AppLocalizationsEn().h2betUnionHistory,
        ),
        'Real Sociedad': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betReal,
          teamLogo: H2BETImage.realLogo,
          teamHistory: AppLocalizationsEn().h2betRealHistory,
        ),
        'Benfica': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betBenfica,
          teamLogo: H2BETImage.benficaLogo,
          teamHistory: AppLocalizationsEn().h2betBenficaHistory,
        ),
        'Copenhagen': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betCopenhagen,
          teamLogo: H2BETImage.copenhagenLogo,
          teamHistory: AppLocalizationsEn().h2betCopenhagenHistory,
        ),
        'Manchester': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betManchester,
          teamLogo: H2BETImage.manchesterLogo,
          teamHistory: AppLocalizationsEn().h2betManchesterHistory,
        ),
        'Bavaria': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betBavaria,
          teamLogo: H2BETImage.bavariaLogo,
          teamHistory: AppLocalizationsEn().h2betBavariaHistory,
        ),
        'Galatasaray': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betGalatasaray,
          teamLogo: H2BETImage.galatasarayLogo,
          teamHistory: AppLocalizationsEn().h2betGalatasarayHistory,
        ),
        'Lance': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betLance,
          teamLogo: H2BETImage.lanceLogo,
          teamHistory: AppLocalizationsEn().h2betLanceHistory,
        ),
        'PSV': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betPSV,
          teamLogo: H2BETImage.psvLogo,
          teamHistory: AppLocalizationsEn().h2betPSVHistory,
        ),
        'Arsenal': FootballTeamH2Bet(
          teamName: AppLocalizationsEn().h2betArsenal,
          teamLogo: H2BETImage.arsenalLogo,
          teamHistory: AppLocalizationsEn().h2betArsenalHistory,
        ),
      };
}
