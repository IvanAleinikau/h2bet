import 'package:h2bet/app/theme/h2bet_image.dart';
import 'package:h2bet/core/model/football_team_h2bet.dart';
import 'package:h2bet/core/model/match_h2bet.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale_en.dart';

class MatchesH2BetRepository {
  List<MatchH2Bet> get matchesH2Bet => [
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betNapoli,
            teamLogo: H2BETImage.napoliLogo,
            teamHistory: AppLocalizationsEn().h2betNapoliHistory,
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betUnion,
            teamLogo: H2BETImage.unionLogo,
            teamHistory: AppLocalizationsEn().h2betUnionHistory,
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 1.23,
          secondTeamCoefficient: 8.01,
          matchPreViewInformation: AppLocalizationsEn().napoliUnionMatch,
          goalInfoFirstTeam: [1, -1, 1, 0],
          goalInfoSecondTeam: [-1, -1, -1, 0],
        ),
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betReal,
            teamLogo: H2BETImage.realLogo,
            teamHistory: AppLocalizationsEn().h2betRealHistory,
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betBenfica,
            teamLogo: H2BETImage.benficaLogo,
            teamHistory: AppLocalizationsEn().h2betBenficaHistory,
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 1.23,
          secondTeamCoefficient: 4.16,
          matchPreViewInformation: AppLocalizationsEn().realBenficaMatch,
          goalInfoFirstTeam: [2, 1, 1, 0],
          goalInfoSecondTeam: [-1, -1, -1, 0],
        ),
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().salzburgh2bet,
            teamLogo: H2BETImage.salzburgLogo,
            teamHistory: '',
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().interh2bet,
            teamLogo: H2BETImage.interLogo,
            teamHistory: '',
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 1.11,
          secondTeamCoefficient: 2.20,
          matchPreViewInformation: AppLocalizationsEn().salInter,
          goalInfoFirstTeam: [1, -1, -1, 0],
          goalInfoSecondTeam: [2, 1, 1, 0],
        ),
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().realMadrid,
            teamLogo: H2BETImage.realMadridLogo,
            teamHistory: '',
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().braga,
            teamLogo: H2BETImage.bragaLogo,
            teamHistory: '',
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 8.01,
          secondTeamCoefficient: 1.20,
          matchPreViewInformation: AppLocalizationsEn().realBraga,
          goalInfoFirstTeam: [1, 1, 1, 0],
          goalInfoSecondTeam: [1, 1, -1, 0],
        ),
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betCopenhagen,
            teamLogo: H2BETImage.copenhagenLogo,
            teamHistory: AppLocalizationsEn().h2betCopenhagenHistory,
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betManchester,
            teamLogo: H2BETImage.manchesterLogo,
            teamHistory: AppLocalizationsEn().h2betManchesterHistory,
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 1.18,
          secondTeamCoefficient: 4.44,
          matchPreViewInformation: AppLocalizationsEn().copMan,
          goalInfoFirstTeam: [1, 2, 2, -1, -1, 0],
          goalInfoSecondTeam: [-1, -1, 1, 0],
        ),
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betBavaria,
            teamLogo: H2BETImage.bavariaLogo,
            teamHistory: AppLocalizationsEn().h2betBavariaHistory,
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betGalatasaray,
            teamLogo: H2BETImage.galatasarayLogo,
            teamHistory: AppLocalizationsEn().h2betGalatasarayHistory,
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 1.73,
          secondTeamCoefficient: 5.80,
          matchPreViewInformation: AppLocalizationsEn().bavGal,
          goalInfoFirstTeam: [1, 1, 1, 0],
          goalInfoSecondTeam: [1, 1, 2, 1, -1, 0],
        ),
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betPSV,
            teamLogo: H2BETImage.psvLogo,
            teamHistory: AppLocalizationsEn().h2betPSVHistory,
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betLance,
            teamLogo: H2BETImage.lanceLogo,
            teamHistory: AppLocalizationsEn().h2betLanceHistory,
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 4.30,
          secondTeamCoefficient: 1.08,
          matchPreViewInformation: AppLocalizationsEn().psvLance,
          goalInfoFirstTeam: [2, 1, -1, 2, 2, 0],
          goalInfoSecondTeam: [2, 1, 2, 0],
        ),
        MatchH2Bet(
          firstFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().h2betArsenal,
            teamLogo: H2BETImage.arsenalLogo,
            teamHistory: AppLocalizationsEn().h2betArsenalHistory,
          ),
          secondFootballTeam: FootballTeamH2Bet(
            teamName: AppLocalizationsEn().seville,
            teamLogo: H2BETImage.sevilleLogo,
            teamHistory: '',
          ),
          matchDateTime: DateTime.now(),
          firstTeamCoefficient: 1.98,
          secondTeamCoefficient: 5.23,
          matchPreViewInformation: AppLocalizationsEn().arsenalSev,
          goalInfoFirstTeam: [1, -1, 1, 0],
          goalInfoSecondTeam: [2, 2, -1, 0],
        ),
      ];
}
