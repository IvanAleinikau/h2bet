import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/h2ber_locale.dart';
import 'package:h2bet/core/di/h2bet_get_it.dart';
import 'package:h2bet/core/router/h2bet_router.dart';
import 'package:flutter/services.dart';
// ignore: depend_on_referenced_packages
import 'package:parse_server_sdk_flutter/parse_server_sdk_flutter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  H2BetGetIt.setupGetIt();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);
  await Parse().initialize(
    'uiHD7xXy23RTfbyX4utdphrdmZh0sPQJS7x9YmR0',
    "https://parseapi.back4app.com/",
    clientKey: '7Hl0f2JXYaOnEmUilrDGKyntrzIEnxH6iHJedMBs',
  );
  runApp(H2BET());
}

class H2BET extends StatelessWidget {
  final _h2BetRouter = H2BetRouter();
  H2BET({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerConfig: _h2BetRouter.config(),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
    );
  }
}
